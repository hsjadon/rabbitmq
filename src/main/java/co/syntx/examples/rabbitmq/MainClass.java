package co.syntx.examples.rabbitmq;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

public class MainClass {
	public MainClass() throws Exception{
		
		QueueConsumer consumer = new QueueConsumer("myqueue");
		Thread consumerThread = new Thread(consumer);
		consumerThread.start();
		
		/*Producer producer = new Producer("myqueue");
		
		for (int i = 0; i < 10; i++) {
			HashMap message = new HashMap();
			message.put("message number", i);
			producer.sendMessage(message);
			System.out.println("Message Number "+ i +" sent.");
		}*/
	}
	
	/**
	 * @param args
	 * @throws SQLException 
	 * @throws IOException 
	 */
	public static void main(String[] args) throws Exception{
	  new MainClass();
	}
}